using System;

class exercise9
{
   static void Main()
   {
      int M, N;

      M = int.Parse(Console.ReadLine());
      N = int.Parse(Console.ReadLine());

      int[,] mat = new int[M, N];
      int[] negativeNums = new int[M*N];

      for (int i = 0; i < M; i++)
      {
         string[] input = Console.ReadLine().Split(' ');

         for (int j = 0; j < N; j++) {
            mat[i, j] = int.Parse(input[j]);

            if (mat[i, j] < 0) {
               negativeNums[j] = mat[i, j];
            }
         }
      }

      Console.WriteLine("VALORES NEGATIVOS: ");
      for (int i = 0; i < negativeNums.Length; i++)
      {
         if (negativeNums[i] != 0) {
            Console.WriteLine(negativeNums[i]);
         }
      }
   }
}
