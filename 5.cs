using System;

class exercise9
{
   static void Main()
   {
      int M = int.Parse(Console.ReadLine());
      int N = int.Parse(Console.ReadLine());

      int[,] A = new int [M, N];
      int[,] B = new int [M, N];
      int[,] C = new int [M, N];

      for (int i = 0; i < M; i++)
      {
         string[] input = Console.ReadLine().Split(' ');

         for (int j = 0; j < N; j++) {
            A[i, j] = int.Parse(input[j]);
         }
      }

      for (int i = 0; i < M; i++)
      {
         string[] input = Console.ReadLine().Split(' ');

         for (int j = 0; j < N; j++) {
            B[i, j] = int.Parse(input[j]);
         }
      }

      for (int i = 0; i < M; i++)
      {
         for (int j = 0; j < N; j++) {
            C[i, j] = A[i, j] + B[i, j];
         }
      }

      for (int i = 0; i < M; i++)
      {
         for (int j = 0; j < N; j++) {
            Console.Write($"{C[i, j]} ");
         }

         Console.WriteLine();
      }
   }
}
