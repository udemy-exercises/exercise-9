using System;

class exercise9
{
   static void Main()
   {
      int N = int.Parse(Console.ReadLine());
      int[,] mat = new int[N, N];
      int biggestNum = 0;
      int[] biggestNumArray = new int[N];

      for (int i = 0; i < N; i++)
      {
         string[] input = Console.ReadLine().Split(' ');

         for (int j = 0; j < N; j++) {
            mat[i, j] = int.Parse(input[j]);
            if (mat[i, j] > biggestNum) {
               biggestNum = mat[i, j];
            }
         }

         biggestNumArray[i] = biggestNum;
         biggestNum = 0;
      }

      for (int i = 0; i < N; i++)
      {
         Console.WriteLine(biggestNumArray[i]);
      }
   }
}
