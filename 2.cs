using System;

class exercise9
{
   static void Main()
   {
      int N = int.Parse(Console.ReadLine());
      int[,] mat = new int[N, N];
      int sum = 0;
      int[] sumArray = new int[N];

      for (int i = 0; i < N; i++)
      {
         string[] input = Console.ReadLine().Split(' ');

         for (int j = 0; j < N; j++) {
            mat[i, j] = int.Parse(input[j]);
            sum += mat[i, j];
         }

         sumArray[i] = sum;
         sum = 0;
      }

      for (int i = 0; i < N; i++)
      {
         Console.WriteLine(sumArray[i]);
      }
   }
}
